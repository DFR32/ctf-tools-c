#include "ctf_tools.h"

// -------------------------------- CTF Wrapper -------------------------------- //

ctf_wrapper ctf_wrapper::apply_and_return(ctf_wrapper& other, std::function<uint8_t(uint8_t, uint8_t)> functor) noexcept
{
    ctf_wrapper return_wrapper(other);

    size_t minimal_size;
    minimal_size = ctf_utils::min<size_t>(other.get_sz(), this->get_sz());

    for(size_t i = 0; i < minimal_size; ++i)
    {
        return_wrapper[i] = functor(other[i], (*this)[i]);
    }

    return return_wrapper;
}

void ctf_wrapper::apply_to_other(ctf_wrapper& other, std::function<uint8_t(uint8_t, uint8_t)> functor) noexcept
{
    size_t minimal_size;
    minimal_size = ctf_utils::min<size_t>(other.get_sz(), this->get_sz());

    for(size_t i = 0; i < minimal_size; ++i)
    {
        other[i] = functor(other[i], (*this)[i]);
    }
}

// --------------------------------- CTF Utils --------------------------------- //

std::string ctf_utils::decode_hex(const std::string& given_string)
{
    char hex_byte[3] = {};
    char hex_byte_as_value = 0;
    std::string result_string;

    if(0 != (given_string.size() & 1)) // check parity bit
        throw std::runtime_error("Error! Tried to decode an odd-length hex string!");

    for(size_t i = 0; i < given_string.size(); i += 2)
    {
        hex_byte[0] = given_string[i];
        hex_byte[1] = given_string[i + 1];

        hex_byte_as_value = static_cast<char>(strtoul(hex_byte, nullptr, 16));
        result_string += hex_byte_as_value;
    }

    return result_string;
}

std::vector<uint8_t> ctf_utils::decode_hex_vector(const std::string& given_string)
{
    char hex_byte[3] = {};
    uint8_t hex_byte_as_value = 0;

    if(0 != (given_string.size() & 1)) // check parity bit
        throw std::runtime_error("Error! Tried to decode an odd-length hex string!");

    std::vector<uint8_t> result_vector = std::vector<uint8_t>(given_string.size() / 2);

    for(size_t i = 0; i < given_string.size(); i += 2)
    {
        hex_byte[0] = given_string[i];
        hex_byte[1] = given_string[i + 1];

        hex_byte_as_value = static_cast<uint8_t>(strtoul(hex_byte, nullptr, 16));
        result_vector.at(i / 2) = hex_byte_as_value;
    }

    return result_vector;
}

std::string ctf_utils::encode_hex(const std::string& given_string) noexcept
{
    if(given_string.empty())
        return std::string();

    static const char hex_mask[] = "0123456789ABCDEF";
    std::string result_string(given_string.size() * 2, 0);

    for(size_t i = 0; i < given_string.size(); ++i)
    {
        result_string[i * 2]        = hex_mask[(given_string[i] >> 4) & 0x0F];
        result_string[i * 2 + 1]    = hex_mask[given_string[i] & 15];
    }

    return result_string;
}

std::string ctf_utils::encode_hex(const std::vector<uint8_t>& given_string) noexcept
{
    if(given_string.empty())
        return std::string();

    static const char hex_mask[] = "0123456789ABCDEF";
    std::string result_string(given_string.size() * 2, 0);

    for(size_t i = 0; i < given_string.size(); ++i)
    {
        result_string[i * 2]        = hex_mask[(given_string[i] >> 4) & 0x0F];
        result_string[i * 2 + 1]    = hex_mask[given_string[i] & 15];
    }

    return result_string;
}