#include "ctf_tools.h"
#include <iostream>
#include <vector>

int main()
{
    ctf_io in;
    in.open("cpp_ctf_tools.exe", std::ios::in | std::ios::binary);
    std::string data = in.read();
    in.close();

    std::cout << data << '\n';

    ctf_io out;
    out.open("cpp_ctf_tools - dup.exe", std::ios::out | std::ios::binary);
    out.write(data);
    out.close();
    return 0;
}