#ifndef CPP_CTF_TOOLS_NETWORKING_HPP
#define CPP_CTF_TOOLS_NETWORKING_HPP

#include <thread>
#include <string>
#include <functional>
#include <exception>
#include <cstdlib>

#define TRANSMISSION_UNIT_SIZE 65536

#if defined(WIN32) || defined(__WIN32) || defined(__WIN32__) || defined(__WIN64) || defined(__WIN64__)
#define CPP_CTF_TOOLS_WINDOWS
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#else
#define CPP_CTF_TOOLS_LINUX
#include <arpa/inet.h>
#endif

#ifdef CPP_CTF_TOOLS_WINDOWS
struct socket_information
{
    SOCKET      _socket;
    sockaddr_in _address;

    enum socket_type
    {
        S_CLIENT,
        S_SERVER,
        S_INVALID
    } socket_type;
};
#else
#endif


// Minimalist TCP client class

#ifdef CPP_CTF_TOOLS_WINDOWS
class tcp_client
{
private:
    socket_information dst;
    int32_t internal_state;

    typedef enum
    {
        NOT_STARTED,
        CANNOT_CONNECT,
        CLIENT_CONNECTED,
        SERVER_LISTENING,
        DISCONNECTED
    } connection_state;

public:
    tcp_client() = default;
    tcp_client(tcp_client& other) = default;

    int32_t connect(const std::string& hostname, const std::string& port)
    {
        int32_t state = 0;
        WSADATA wsa;

        struct addrinfo *result = nullptr, *ptr = nullptr, hints{};

        state = WSAStartup(MAKEWORD(2,2), &wsa);

        if(state != 0)
            throw std::runtime_error("TCP_CLIENT connect error! Cannot start Windows Sockets Application.");

        memset(&hints, 0, sizeof(hints));

        hints.ai_family     = AF_UNSPEC;
        hints.ai_socktype   = SOCK_STREAM;
        hints.ai_protocol   = IPPROTO_TCP;

        state = getaddrinfo(&hostname[0], &port[0], &hints, &result);

        if(state != 0)
            throw std::runtime_error(std::string("TCP_CLIENT connect error! Could not get the address information! WSA error code: ") +
                                     std::to_string(WSAGetLastError()));

        for(ptr = result; ptr != nullptr; ptr = ptr->ai_next)
        {
            dst._socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

            if(dst._socket == INVALID_SOCKET)
            {
                WSACleanup();
                throw std::runtime_error(std::string("TCP_CLIENT connect error! socket() failed with error: ") +
                                         std::to_string(WSAGetLastError()));
            }

            // Careful with the scope here! "connect" is a class method
            // ... in this scope!
            state = ::connect(dst._socket, ptr->ai_addr, static_cast<int32_t>(ptr->ai_addrlen));

            if(state == SOCKET_ERROR)
            {
                closesocket(dst._socket);
                dst._socket = INVALID_SOCKET;
                continue;
            }

            break;
        }

        freeaddrinfo(result);

        // Return the appropriate enum
        if(dst._socket == INVALID_SOCKET)
        {
            internal_state = connection_state::CANNOT_CONNECT;
            return connection_state::CANNOT_CONNECT;
        }

        internal_state = connection_state::CLIENT_CONNECTED;
        return connection_state::CLIENT_CONNECTED;
    }

    int32_t send(const std::string& buffer, int32_t packet_flags = 0)
    {
        int32_t state       = 0;

        // Prevent malformed packets with negative sizes
        auto buffer_size = static_cast<int32_t>(buffer.size());

        if(buffer_size < 0)
            throw std::runtime_error("TCP_CLIENT send critical error! Attempted to send a packet with negative length");

        if(internal_state != connection_state::CLIENT_CONNECTED)
            throw std::runtime_error("TCP_CLIENT send error! Cannot send with an unconnected socket.");

        state = ::send(dst._socket, buffer.c_str(), static_cast<int32_t>(buffer.size()), packet_flags);

        if(state == SOCKET_ERROR)
            throw std::runtime_error(std::string("TCP_CLIENT send error! ") + std::to_string(WSAGetLastError()));

        return state;
    }

    std::string recv(int32_t recv_flags = 0)
    {
        std::string buffer(TRANSMISSION_UNIT_SIZE, 0);

        int32_t state = 0;

        if(internal_state != connection_state::CLIENT_CONNECTED)
            throw std::runtime_error("TCP_CLIENT recv error! Cannot receive with an unconnected socket.");

        state = ::recv(dst._socket, &buffer[0], TRANSMISSION_UNIT_SIZE - 1, recv_flags);

        if(state == SOCKET_ERROR)
            throw std::runtime_error(std::string("TCP_CLIENT recv error! ") + std::to_string(WSAGetLastError()));


        if(state < 0)
            throw std::runtime_error("TCP_CLIENT recv error! state is negative!");

        buffer.resize(static_cast<size_t>(state));
        return buffer;
    }
};
#else
#endif

#endif //CPP_CTF_TOOLS_NETWORKING_HPP
