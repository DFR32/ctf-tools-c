#include "ctf_tools.h"

static constexpr int64_t io_upper_size_limit = 0xFFFFFF00;

void ctf_io::open(const std::string& path, std::ios::openmode mode)
{
    if(!io_file.good())
        throw std::runtime_error("Error! File instance failed the \"good\" check!");

    if(!io_file.is_open())
        throw std::runtime_error("Error! File was not opened!");
}

std::string ctf_io::read()
{
    if(!io_file.good())
        throw std::runtime_error("Error! Cannot read from file that failed the \"good\" check!");

    if(!io_file.is_open())
        throw std::runtime_error("Error! Cannot read from a file that is not opened!");

    // Go to the end and find the position
    // .. this determines the file size
    io_file.seekg(0, std::ios::end);
    int64_t file_size = io_file.tellg();
    io_file.seekg(0, std::ios::beg);

    // Hopefully we are dealing with small files...
    if(file_size > io_upper_size_limit)
        throw std::runtime_error("Error! I/O does not support files of such size.");

    // Create a buffer to store the data
    std::string result_string(static_cast<size_t>(file_size), 0);

    // Read into the buffer
    io_file.read(&result_string[0], file_size);

    return result_string;
}

void ctf_io::write(std::string& buffer)
{
    if(!io_file.good())
        throw std::runtime_error("Error! Cannot write to a file that failed the \"good\" check!");

    if(!io_file.is_open())
        throw std::runtime_error("Error! Cannot write to a file that is not opened!");

    if(buffer.empty())
        return;

    io_file.write(buffer.c_str(), buffer.size());
}

void ctf_io::close()
{
    if(io_file.is_open())
        io_file.close();
    else throw std::runtime_error("Error! Attempted to close unopened file!");
}