#ifndef CPP_CTF_TOOLS_CTF_TOOLS_H
#define CPP_CTF_TOOLS_CTF_TOOLS_H

#include <utility>
#include <vector>
#include <cmath>
#include <string>
#include <cstdint>
#include <stdexcept>
#include <functional>
#include <fstream>

class ctf_wrapper
{
private:
    std::vector<uint8_t> local_data;

public:
    // Default constructor
    ctf_wrapper() = default;

    // Default copy constructor
    ctf_wrapper(const ctf_wrapper& other) = default;

    // -------------------- Constructors -------------------- //

    explicit ctf_wrapper(std::string tmp_string) noexcept
    {
        for(char& tmp_char : tmp_string)
            local_data.push_back( static_cast<uint8_t>(tmp_char) );
    }

    explicit ctf_wrapper(std::vector<uint8_t> tmp_vector) noexcept
        : local_data(std::move(tmp_vector))
    {}

    // -------------------- Operators -------------------- //

    explicit operator std::string()
    {
        std::string result(local_data.size(), 0);

        for(size_t i = 0; i < local_data.size(); ++i)
            result.at(i) = static_cast<char>(local_data.at(i));

        return result;
    }

    explicit operator std::vector<uint8_t>()
    {
        return local_data;
    }

    uint8_t& operator[] (size_t idx)
    {
        return local_data.at(idx);
    }

    // -------------------- Functions -------------------- //

    // ----------------- Vector functions ----------------- //

    std::vector<uint8_t>& v()
    {
        return local_data;
    }

    size_t get_sz()
    {
        return local_data.size();
    }

    // ----------------- String functions ----------------- //

    std::string s()
    {
        std::string result(local_data.size(), 0);

        for(size_t i = 0; i < local_data.size(); ++i)
            result.at(i) = static_cast<char>(local_data.at(i));

        return result;
    }

    // ------------------ Object functions ------------------ //

    ctf_wrapper apply_and_return(ctf_wrapper& other, std::function<uint8_t(uint8_t, uint8_t)> functor)  noexcept;
    void        apply_to_other(ctf_wrapper& other, std::function<uint8_t(uint8_t, uint8_t)> functor)    noexcept;
};

class ctf_io
{
private:
    std::fstream io_file;

public:

    // Default constructor
    ctf_io() = default;

    void open(const std::string& path, std::ios::openmode mode);
    std::string read();
    void write(std::string& buffer);
    void close();
};


/*class ctf_sock
{
private:

};*/

namespace ctf_utils
{
    std::string decode_hex(const std::string& given_string);
    std::vector<uint8_t> decode_hex_vector(const std::string& given_string);

    std::string encode_hex(const std::string& given_string)           noexcept;
    std::string encode_hex(const std::vector<uint8_t>& given_vector)  noexcept;

    template <typename T>
    T min(T p1, T p2)
    {
        return (p1 > p2) ? p2 : p1;
    }

    std::string base64_encode(const std::string& bytes_to_encode)      noexcept;
    std::string base64_decode(const std::string& bytes_to_decode)      noexcept;
}

#endif //CPP_CTF_TOOLS_CTF_TOOLS_H
