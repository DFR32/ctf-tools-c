#include "ctf_tools.h"
#include "networking.hpp"
#include <iostream>
#include <vector>

int main()
{
    tcp_client netcat_client{};
    netcat_client.connect("www.ltni.ro", "80");

    netcat_client.send(
            "GET / HTTP/1.1\r\n"
            "Host: ltni.ro\r\n"
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0\r\n\r\n"
            );

    std::cout << netcat_client.recv() << '\n';
    return 0;
}